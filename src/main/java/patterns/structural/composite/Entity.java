package patterns.structural.composite;

public interface Entity {
    int getSize();
}
