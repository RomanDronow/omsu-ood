package patterns.structural.composite;

public class Block implements Entity {
    @Override
    public int getSize() {
        return 4;
    }

    public Block() {
    }
}
