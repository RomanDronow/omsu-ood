package patterns.structural.composite;

public class Demo {
    public static void main(String[] args) {
        Composite c = new Composite();
        Block b = new Block();
        Sphere s = new Sphere();
        c.add(b);
        c.add(s);
        System.out.println(c.getSize());
    }
}
