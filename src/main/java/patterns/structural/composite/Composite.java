package patterns.structural.composite;

public class Composite {
    class Node {
        Entity e;
        Node next;

        Node(Entity e) {
            this.e = e;
        }

        Node() {
            e = null;
        }
    }

    private Node node;

    public Composite() {
        node = new Node();
    }

    public void add(Entity e) {
        if (node.e == null) {
            node.e = e;
        } else {
            Node temp = node;
            while (temp.next != null) {
                temp = temp.next;
            }
            temp.next = new Node(e);
        }
    }

    public int getSize() {
        int result = 0;
        Node temp = node;
        while (temp != null) {
            result += temp.e.getSize();
            temp = temp.next;
        }
        return result;
    }
}
