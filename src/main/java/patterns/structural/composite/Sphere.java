package patterns.structural.composite;

public class Sphere implements Entity {
    @Override
    public int getSize() {
        return 2;
    }

    public Sphere() {
    }
}
