package patterns.structural.proxy;

public class Solver implements ISolver {
    @Override
    public double[] solve(double a, double b, double c) {
        double[] res = new double[2];
        double d = b * b - 4 * a * c;
        if (d < 0) {
            System.out.println("Корней нет");
        } else {
            if (d == 0) {
                res[0] = -b / (2 * a);
                res[1] = res[0];
            } else {
                res[0] = b / (2 * a);
                res[1] = -res[0];
            }
        }
        return res;
    }
}
