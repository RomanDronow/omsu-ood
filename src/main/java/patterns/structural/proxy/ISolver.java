package patterns.structural.proxy;

public interface ISolver {
    double[] solve(double a, double b, double c);
}
