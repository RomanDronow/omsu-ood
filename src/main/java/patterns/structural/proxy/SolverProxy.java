package patterns.structural.proxy;

public class SolverProxy implements ISolver {
    private Solver solver;

    @Override
    public double[] solve(double a, double b, double c) {
        if(solver == null) solver = new Solver();
        return solver.solve(a,b,c);
    }
}
