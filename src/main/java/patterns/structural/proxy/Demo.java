package patterns.structural.proxy;

public class Demo {
    public static void main(String[] args) {
        ISolver s = new SolverProxy();
        double[] dd = s.solve(1,1,-6);
        for (double d : dd) {
            System.out.println(d);
        }
    }
}
