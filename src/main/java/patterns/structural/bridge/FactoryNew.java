package patterns.structural.bridge;

public class FactoryNew implements Quality {
    protected Cloth cloth;

    public FactoryNew(Cloth cloth) {
        this.cloth = cloth;
    }

    @Override
    public String check() {
        return cloth.name() + " It's factory new.";
    }
}
