package patterns.structural.bridge;

public interface Quality {
    String check();
}
