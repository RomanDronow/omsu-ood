package patterns.structural.bridge;

public class Dress  implements Cloth{
    @Override
    public void wear() {
        System.out.println("You're wearing dress.");
    }

    @Override
    public void takeOff() {
        System.out.println("You're taking off your dress.");
    }

    @Override
    public String name() {
        return "This is dress.";
    }
}
