package patterns.structural.bridge;

public interface Cloth {
    void wear();
    void takeOff();
    String name();
}
