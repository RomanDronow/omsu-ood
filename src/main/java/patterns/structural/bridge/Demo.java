package patterns.structural.bridge;

public class Demo {
    public static void main(String[] args) {
        Cloth shirt = new Shirt();
        Cloth dress = new Dress();
        testCloth(shirt);
        testCloth(dress);
    }

    public static void testCloth(Cloth cloth) {
        System.out.println("Tests with factory new cloth.");
        Quality fn = new FactoryNew(cloth);
        System.out.println(fn.check());
        System.out.println();
        System.out.println("Tests with well worn cloth.");
        Quality ww = new WellWorn(cloth);
        System.out.println(ww.check());
        System.out.println();
    }
}
