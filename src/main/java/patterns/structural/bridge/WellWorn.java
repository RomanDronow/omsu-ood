package patterns.structural.bridge;

public class WellWorn extends FactoryNew {

    @Override
    public String check() {
        return cloth.name() + " It's well worn.";
    }

    public WellWorn(Cloth cloth) {
        super(cloth);
    }
}
