package patterns.structural.bridge;

public class Shirt implements Cloth {
    @Override
    public void wear() {
        System.out.println("You're wearing shirt.");
    }

    @Override
    public void takeOff() {
        System.out.println("You're taking off your shirt.");
    }

    @Override
    public String name() {
        return "This is shirt.";
    }
}
