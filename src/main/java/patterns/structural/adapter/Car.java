package patterns.structural.adapter;

public class Car {
    private int wheels;
    private String name;

    public Car(int wheels, String name) {
        this.wheels = wheels;
        this.name = name;
    }

    public String getName() {

        return name;
    }

    public int getWheels() {
        return wheels;
    }

    public Car(String name) {
        this.name = name;
        wheels = 4;
    }

    public Car(int wheels) {
        name = "Just car";
        this.wheels = wheels;
    }

    public Car() {
        name = "Just car";
        wheels = 4;
    }
}
