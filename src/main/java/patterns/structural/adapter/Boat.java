package patterns.structural.adapter;

public class Boat {
    private String name;

    public Boat() {
        name = "Just boat";
    }

    public String getName() {
        return name;
    }

    public Boat(String name) {

        this.name = name;
    }
}
