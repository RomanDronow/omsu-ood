package patterns.structural.adapter;

import static patterns.structural.adapter.Client.checkWheels;

public class Demo {
    public static void main(String[] args) {
        Car car = new Car();
        Boat boat = new Boat();
        Mechanic mechanic = new Mechanic();
        if(checkWheels(car)){
            System.out.println("Success");
        }
        Car adapted = mechanic.adapt(boat);
        if(checkWheels(adapted)){
            System.out.println("And another one");
        }
    }

}
