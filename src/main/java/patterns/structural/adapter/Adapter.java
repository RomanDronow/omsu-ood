package patterns.structural.adapter;

public interface Adapter {
    Car adapt(Boat boat);
}
