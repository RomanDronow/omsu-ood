package patterns.structural.adapter;

public class Client {

    public static boolean checkWheels(Car car){
        if(car.getWheels() != 4) return false;
        return true;
    }
}
