package patterns.structural.adapter;

public class Mechanic implements Adapter{
    public Car adapt(Boat boat){
        return new Car(boat.getName());
    }
}
