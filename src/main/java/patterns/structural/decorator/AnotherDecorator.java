package patterns.structural.decorator;

public class AnotherDecorator implements Decorator{
    private Job job;
    private Decorator decorator;

    public AnotherDecorator(Job job){
        this.job = job;
    }

    public AnotherDecorator(Job job, Decorator decorator) {
        this.job = job;
        this.decorator = decorator;
    }

    @Override
    public String print() {
        String s = "";
        if(decorator != null){
            s = decorator.print();
        }
        return job.print() + "Another " + s;
    }
}
