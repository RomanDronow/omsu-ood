package patterns.structural.decorator;

public interface Decorator {
    String print();
}
