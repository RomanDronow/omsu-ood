package patterns.structural.decorator;

public class Demo {
    public static void main(String[] args) {
        Job job = new Doctor();
        Decorator s = new SimpleDecorator(job);
        Decorator a = new AnotherDecorator(job,s);
        System.out.println(s.print());
        System.out.println(a.print());
        Decorator b = new AnotherDecorator(job,a);
        System.out.println(b.print());
    }
}
