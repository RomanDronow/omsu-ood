package patterns.structural.decorator;

public interface Job {
    String print();
}
