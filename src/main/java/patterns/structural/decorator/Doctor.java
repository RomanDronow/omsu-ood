package patterns.structural.decorator;

public class Doctor implements Job {
    @Override
    public String print() {
        return "Doctor";
    }
}
