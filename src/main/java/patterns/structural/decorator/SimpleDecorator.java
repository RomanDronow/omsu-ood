package patterns.structural.decorator;

public class SimpleDecorator implements Decorator{
    private Job job;
    private Decorator decorator;

    public SimpleDecorator(Job job){
        this.job = job;
    }

    public SimpleDecorator(Job job, Decorator decorator) {
        this.job = job;
        this.decorator = decorator;
    }

    @Override
    public String print() {
        String s = "";
        if(decorator != null){
            s = decorator.print();
        }
        return job.print() + "Simple " + s;
    }
}
