package patterns.structural.flyweight;

public class SymbolB extends Symbol {
    public SymbolB() {
        symbol = 'B';
    }

    @Override
    public void print() {
        System.out.print(symbol);
    }
}
