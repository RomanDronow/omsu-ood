package patterns.structural.flyweight;

import java.util.HashMap;
import java.util.Map;

public class SymbolFactory {
    private Map<Integer, Symbol> symbols = new HashMap<>();

    public Symbol getSymbol(int key) {
        Symbol s = symbols.get(key);
        if (s == null) {
            if (key % 2 == 0) {
                s = new SymbolA();
            } else {
                s = new SymbolB();
            }
        }
        symbols.put(key, s);
        return s;
    }
}
