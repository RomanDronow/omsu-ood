package patterns.structural.flyweight;

public class Demo {
    public static void main(String[] args) {
        SymbolFactory factory = new SymbolFactory();

        int[] keys = {1,2,3,4,5,6,7,8,4,3,2,2,1,4,4,55,65,6};
        for (int key : keys) {

            Symbol s = factory.getSymbol(key);
            s.print();
        }
    }
}
