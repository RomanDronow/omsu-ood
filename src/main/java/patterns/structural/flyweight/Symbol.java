package patterns.structural.flyweight;

public abstract class Symbol {
    protected char symbol;
    public abstract void print();
}
