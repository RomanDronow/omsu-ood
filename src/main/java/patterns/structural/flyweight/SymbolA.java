package patterns.structural.flyweight;

public class SymbolA extends Symbol {
    public SymbolA() {
        symbol = 'A';
    }

    @Override
    public void print() {
        System.out.print(symbol);
    }
}
