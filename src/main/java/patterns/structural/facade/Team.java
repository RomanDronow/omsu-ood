package patterns.structural.facade;

public class Team {
    private Driver d;
    private Operator o;
    private Worker w;

    public Team(Driver d, Operator o, Worker w) {
        this.d = d;
        this.o = o;
        this.w = w;
    }

    public void print() {
        System.out.println(d.getClass().getSimpleName() + ": " + d.drive());
        System.out.println(o.getClass().getSimpleName() + ": " + o.operate());
        System.out.println(w.getClass().getSimpleName() + ": " + w.work());
    }
}
