package patterns.structural.facade;

public class Driver {
    public String drive(){
        return "I drive";
    }
}
