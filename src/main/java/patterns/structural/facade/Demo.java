package patterns.structural.facade;

public class Demo {
    public static void main(String[] args) {
        Team team = new Team(new Driver(), new Operator(), new Worker());
        team.print();
    }
}
