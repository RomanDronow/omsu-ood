package patterns.structural.facade;

public class Worker {
    public String work(){
        return "I work";
    }
}
