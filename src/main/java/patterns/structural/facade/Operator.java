package patterns.structural.facade;

public class Operator {
    public String operate(){
        return "I operate";
    }
}
