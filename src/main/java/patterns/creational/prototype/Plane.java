package patterns.creational.prototype;

public class Plane extends Aircraft {
    public boolean isReactive;

    public Plane(){}

    public Plane(Plane other){
        super(other);
        if(other != null){
            this.isReactive = other.isReactive;
        }
    }

    @Override
    public Aircraft clone() {
        return new Plane(this);
    }

    @Override
    public boolean equals(Object object2) {
        if (!(object2 instanceof Plane) || !super.equals(object2)) return false;
        Plane plane2 = (Plane) object2;
        return plane2.isReactive == isReactive;
    }
}
