package patterns.creational.prototype;

public class Dirigible extends Aircraft {
    public double volume;

    public Dirigible() {
    }

    public Dirigible(Dirigible other) {
        super(other);
        if (other != null) {
            this.volume = other.volume;
        }
    }

    @Override
    public Aircraft clone() {
        return new Dirigible(this);
    }

    @Override
    public boolean equals(Object object2) {
        if (!(object2 instanceof Dirigible) || !super.equals(object2)) return false;
        Dirigible dirigible2 = (Dirigible) object2;
        return dirigible2.volume == volume;
    }
}
