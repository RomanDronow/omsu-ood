package patterns.creational.prototype;

import java.util.ArrayList;
import java.util.List;

public class Demo {
    public static void main(String[] args) {
        List<Aircraft> aircrafts = new ArrayList<>();
        List<Aircraft> aircrafts2 = new ArrayList<>();

        Plane plane = new Plane();
        plane.isReactive = true;

        aircrafts.add(plane);

        Plane anotherPlane = (Plane) plane.clone();
        aircrafts.add(anotherPlane);

        Dirigible dirigible = new Dirigible();
        dirigible.volume = 5000;
        aircrafts.add(dirigible);

        cloneAndCompare(aircrafts, aircrafts2);
    }

    private static void cloneAndCompare(List<Aircraft> aircrafts, List<Aircraft> aircrafts2) {
        for (Aircraft aircraft : aircrafts) {
            aircrafts2.add(aircraft.clone());
        }

        for (int i = 0; i < aircrafts.size(); i++) {
            if (aircrafts.get(i) != aircrafts2.get(i)) {
                System.out.println(i + ": Aircrafts are different objects");
                if (aircrafts.get(i).equals(aircrafts2.get(i))) {
                    System.out.println(i + ": And they are identical");
                } else {
                    System.out.println(i + ": But they are not identical");
                }
            } else {
                System.out.println(i + ": Aircraft objects are the same");
            }
        }
    }
}
