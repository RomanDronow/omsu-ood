package patterns.creational.prototype;

import java.util.Objects;

public abstract class Aircraft {
    public String material;
    public boolean isMilitary;
    public int weigth;

    public Aircraft() {
    }

    public Aircraft(Aircraft other) {
        if (other != null) {
            this.material = other.material;
            this.weigth = other.weigth;
            this.isMilitary = other.isMilitary;
        }
    }

    public abstract Aircraft clone();

    @Override
    public boolean equals(Object object2) {
        if (this == object2) return true;
        if (object2 == null || getClass() != object2.getClass()) return false;
        Aircraft aircraft = (Aircraft) object2;
        return isMilitary == aircraft.isMilitary &&
                weigth == aircraft.weigth &&
                Objects.equals(material, aircraft.material);
    }
}
