package patterns.creational.builder.builders;

import patterns.creational.builder.computers.Type;
import patterns.creational.builder.details.CPU;
import patterns.creational.builder.details.Display;
import patterns.creational.builder.details.GFXCard;
import patterns.creational.builder.details.RAM;

public interface Builder {
    void setType(Type type);

    void setCPU(CPU cpu);

    void setRAM(RAM ram);

    void setDisplay(Display display);

    void setGFXCard(GFXCard gfxCard);
}
