package patterns.creational.builder.builders;

import patterns.creational.builder.computers.Computer;
import patterns.creational.builder.computers.Type;
import patterns.creational.builder.details.CPU;
import patterns.creational.builder.details.Display;
import patterns.creational.builder.details.GFXCard;
import patterns.creational.builder.details.RAM;

public class ComputerBuilder implements Builder {

    private Type type;
    private CPU cpu;
    private RAM ram;
    private Display display;
    private GFXCard gfxCard;

    @Override
    public void setType(Type type) {
        this.type = type;
    }

    @Override
    public void setCPU(CPU cpu) {
        this.cpu = cpu;
    }

    @Override
    public void setRAM(RAM ram) {
        this.ram = ram;
    }

    @Override
    public void setDisplay(Display display) {
        this.display = display;
    }

    @Override
    public void setGFXCard(GFXCard gfxCard) {
        this.gfxCard = gfxCard;
    }

    public Computer build() {
        return new Computer(type, cpu, ram, display, gfxCard);
    }
}
