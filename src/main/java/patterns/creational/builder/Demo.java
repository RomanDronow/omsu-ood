package patterns.creational.builder;

import patterns.creational.builder.builders.ComputerBuilder;
import patterns.creational.builder.builders.SpecificationsBuilder;
import patterns.creational.builder.computers.Computer;
import patterns.creational.builder.computers.Specifications;
import patterns.creational.builder.director.Director;

public class Demo {
    public static void main(String[] args) {
        Director director = new Director();
        ComputerBuilder computerBuilder = new ComputerBuilder();
        director.buildOfficePC(computerBuilder);
        Computer computer = computerBuilder.build();
        System.out.println("Computer built: " + computer.getType());

        SpecificationsBuilder specificationsBuilder = new SpecificationsBuilder();

        director.buildOfficePC(specificationsBuilder);
        Specifications specifications = specificationsBuilder.build();
        System.out.println("Specifications:\n"+ specifications.print());
    }
}
