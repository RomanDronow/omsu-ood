package patterns.creational.builder.director;

import patterns.creational.builder.builders.Builder;
import patterns.creational.builder.computers.Type;
import patterns.creational.builder.details.CPU;
import patterns.creational.builder.details.Display;
import patterns.creational.builder.details.GFXCard;
import patterns.creational.builder.details.RAM;

public class Director {
    public void buildOfficePC(Builder builder) {
        builder.setType(Type.MONOBLOCK);
        builder.setCPU(new CPU(2.7, 2, false));
        builder.setRAM(new RAM(4, 3));
        builder.setDisplay(new Display(15, 1280, 1024));
        builder.setGFXCard(new GFXCard(null, 0));
    }

    public void buildGamingPC(Builder builder) {
        builder.setType(Type.DESKTOP);
        builder.setCPU(new CPU(4.0, 8, true));
        builder.setRAM(new RAM(16, 4));
        builder.setDisplay(new Display(22, 2560, 1440));
        builder.setGFXCard(new GFXCard("AMD", 8));
    }

    public void buildUltrabook(Builder builder) {
        builder.setType(Type.LAPTOP);
        builder.setCPU(new CPU(3.6, 4, false));
        builder.setRAM(new RAM(8, 4));
        builder.setDisplay(new Display(14, 1920, 1080));
        builder.setGFXCard(new GFXCard("Intel", 2));
    }
}
