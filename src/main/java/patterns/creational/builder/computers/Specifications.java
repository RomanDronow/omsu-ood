package patterns.creational.builder.computers;

import patterns.creational.builder.details.CPU;
import patterns.creational.builder.details.Display;
import patterns.creational.builder.details.GFXCard;
import patterns.creational.builder.details.RAM;

public class Specifications {
    private final Type type;
    private final CPU cpu;
    private final RAM ram;
    private final Display display;
    private final GFXCard gfxCard;

    public Specifications(Type type, CPU cpu, RAM ram, Display display, GFXCard gfxCard) {
        this.type = type;
        this.cpu = cpu;
        this.ram = ram;
        this.display = display;
        this.gfxCard = gfxCard;
    }

    public String print() {
        String res = "";
        res += "Formfactor: " + type;
        res += "\nCPU: " + cpu.getCores() + " cores at " + cpu.getFrequency() + " GHz";
        res += "\nRAM: DDR" + ram.getDDRType() + " with " + ram.getVolume() + " GB";
        res += "\nDisplay: " + display.getInches() + " inches, up to " + display.getWidth() + "x" + display.getHeight() + " resolution";
        res += "\nGraphics card: ";
        if (gfxCard.getManufacturer() == null || gfxCard.getVRAM() == 0) {
            res += "none";
        } else {
            res += gfxCard.getManufacturer() + " with " + gfxCard.getVRAM() + " GB of VRAM";
        }
        return res;
    }
}
