package patterns.creational.builder.computers;

public enum Type {
    DESKTOP, LAPTOP, MONOBLOCK
}
