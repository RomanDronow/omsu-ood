package patterns.creational.builder.computers;

import patterns.creational.builder.details.CPU;
import patterns.creational.builder.details.Display;
import patterns.creational.builder.details.GFXCard;
import patterns.creational.builder.details.RAM;

public class Computer {
    private final Type type;
    private final CPU cpu;
    private final RAM ram;
    private final Display display;
    private final GFXCard gfxCard;

    public Computer(Type type, CPU cpu, RAM ram, Display display, GFXCard gfxCard) {
        this.type = type;
        this.cpu = cpu;
        this.ram = ram;
        this.display = display;
        this.gfxCard = gfxCard;
    }

    public Type getType() {
        return type;
    }

    public CPU getCpu() {
        return cpu;
    }

    public RAM getRam() {
        return ram;
    }

    public Display getDisplay() {
        return display;
    }

    public GFXCard getGfxCard() {
        return gfxCard;
    }
}
