package patterns.creational.builder.details;

public class GFXCard {
    private String manufacturer;
    private int VRAM;

    public GFXCard(String manufacturer, int VRAM) {
        this.manufacturer = manufacturer;
        this.VRAM = VRAM;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public int getVRAM() {
        return VRAM;
    }
}
