package patterns.creational.builder.details;

public class RAM {
    private int volume;
    private int DDRType;

    public int getVolume() {
        return volume;
    }

    public int getDDRType() {
        return DDRType;
    }

    public RAM(int volume, int DDRType) {
        this.volume = volume;
        this.DDRType = DDRType;
    }
}
