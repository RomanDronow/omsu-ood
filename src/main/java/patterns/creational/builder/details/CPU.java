package patterns.creational.builder.details;

public class CPU {
    private double frequency;
    private final int cores;

    public double getFrequency() {
        return frequency;
    }

    public int getCores() {
        return cores;
    }

    public CPU(double frequency, int cores, boolean allowOverclock) {
        this.frequency = frequency;
        this.cores = cores;
    }
}
