package patterns.creational.builder.details;

public class Display {
    private double inches;
    private int width;
    private int height;

    public Display(double inches, int width, int height) {
        this.inches = inches;
        this.width = width;
        this.height = height;
    }

    public double getInches() {
        return inches;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
}
