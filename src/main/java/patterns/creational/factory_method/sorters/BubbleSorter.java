package patterns.creational.factory_method.sorters;

import patterns.creational.factory_method.sorts.BubbleSort;
import patterns.creational.factory_method.sorts.Sort;

public class BubbleSorter extends Sorter{
    @Override
    public Sort factoryMethod() {
        return new BubbleSort();
    }
}
