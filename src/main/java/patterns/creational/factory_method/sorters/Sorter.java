package patterns.creational.factory_method.sorters;

import patterns.creational.factory_method.sorts.Sort;

public abstract class Sorter {
    public abstract Sort factoryMethod();
}
