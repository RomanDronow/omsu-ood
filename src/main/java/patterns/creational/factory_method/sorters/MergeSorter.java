package patterns.creational.factory_method.sorters;

import patterns.creational.factory_method.sorts.MergeSort;
import patterns.creational.factory_method.sorts.Sort;

public class MergeSorter extends Sorter {
    @Override
    public Sort factoryMethod() {
        return new MergeSort();
    }
}
