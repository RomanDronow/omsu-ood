package patterns.creational.factory_method.sorters;

import patterns.creational.factory_method.sorts.ShellSort;
import patterns.creational.factory_method.sorts.Sort;

public class ShellSorter extends Sorter {
    @Override
    public Sort factoryMethod() {
        return new ShellSort();
    }
}
