package patterns.creational.factory_method.sorts;

public interface Sort {
    public void sort(double ... array);
}
