package patterns.creational.factory_method;

import patterns.creational.factory_method.sorters.BubbleSorter;
import patterns.creational.factory_method.sorters.MergeSorter;
import patterns.creational.factory_method.sorters.ShellSorter;
import patterns.creational.factory_method.sorters.Sorter;
import patterns.creational.factory_method.sorts.Sort;

import java.util.Scanner;

public class Demo {
    public static void main(String[] args) {
        double[] array = new double[20];
        for (int i = 0; i < array.length; i++) {
            array[i] = Math.pow(i, 2) / (5 * Math.pow(-1, i));
            System.out.print(array[i] + " ");
        }
        System.out.println("\n1. Bubble sort\n2. Merge sort\n3. Shell sort");
        Sorter sorter = null;
        Scanner scanner = new Scanner(System.in);
        int key = scanner.nextInt();
        if (key == 1) {
            sorter = new BubbleSorter();
            System.out.println("bubble\n");
        }
        if (key == 2) {
            sorter = new MergeSorter();
            System.out.println("merge\n");
        }
        if (key == 3) {
            sorter = new ShellSorter();
            System.out.println("shell\n");
        }
        Sort sortMethod = null;
        if (sorter != null) {
            sortMethod = sorter.factoryMethod();
        }
        if (sortMethod != null) {
            sortMethod.sort(array);
        }
        for (double anArray : array) {
            System.out.print(anArray + " ");
        }
    }
}

