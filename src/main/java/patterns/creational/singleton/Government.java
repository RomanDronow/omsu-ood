package patterns.creational.singleton;

public class Government {
    private static Government government;
    public String head;

    private Government(String head) {
        this.head = head;
    }

    public static Government getInstance(String head) {
        if(government == null){
            government = new Government(head);
        }
        return government;
    }
}
