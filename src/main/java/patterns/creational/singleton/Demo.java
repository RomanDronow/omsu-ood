package patterns.creational.singleton;

public class Demo {
    public static void main(String[] args) {
        Government government1 = Government.getInstance("empress");
        Government government2 = Government.getInstance("president");
        System.out.println(government1.head);
        System.out.println(government2.head);
    }
}
