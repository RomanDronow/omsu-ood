package patterns.creational.abstract_factory;

import patterns.creational.abstract_factory.factories.ArcherFactory;
import patterns.creational.abstract_factory.factories.MageFactory;
import patterns.creational.abstract_factory.factories.RPGFactory;
import patterns.creational.abstract_factory.factories.WarriorFactory;

import java.util.Scanner;

public class Demo {
    private static Game configureGame(){
        Game game;
        RPGFactory factory;
        Scanner sc = new Scanner(System.in);
        System.out.println("1 for archer, 2 for warrior,3 for mage");
        int i = sc.nextInt();
        if(i == 1){
            factory = new ArcherFactory();
            game = new Game(factory);
        } else
        if(i == 2){
            factory = new WarriorFactory();
            game = new Game(factory);
        } else
        if(i == 3){
            factory = new MageFactory();
            game = new Game(factory);
        } else {
            System.out.println("Wrong pick");
            return null;
        }
        return game;
    }

    public static void main(String[] args) {
        Game game = configureGame();
        game.pick();
    }
}
