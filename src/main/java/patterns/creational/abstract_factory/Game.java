package patterns.creational.abstract_factory;

import patterns.creational.abstract_factory.factories.RPGFactory;
import patterns.creational.abstract_factory.weapons.Archetype;
import patterns.creational.abstract_factory.weapons.Weapon;

public class Game {
    private Archetype archetype;
    private Weapon weapon;

    public Game(RPGFactory factory){
        archetype = factory.createArchetype();
        weapon = factory.createWeapon();
    }

    public void pick(){
        archetype.pick();
        weapon.pick();
    }
}
