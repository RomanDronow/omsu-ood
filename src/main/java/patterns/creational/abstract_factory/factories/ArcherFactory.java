package patterns.creational.abstract_factory.factories;

import patterns.creational.abstract_factory.weapons.Archer;
import patterns.creational.abstract_factory.weapons.Archetype;
import patterns.creational.abstract_factory.weapons.Bow;
import patterns.creational.abstract_factory.weapons.Weapon;

public class ArcherFactory implements RPGFactory {
    @Override
    public Archetype createArchetype() {
        return new Archer();
    }

    @Override
    public Weapon createWeapon() {
        return new Bow();
    }
}
