package patterns.creational.abstract_factory.factories;

import patterns.creational.abstract_factory.weapons.Archetype;
import patterns.creational.abstract_factory.weapons.Weapon;

public interface RPGFactory {
    Archetype createArchetype();
    Weapon createWeapon();
}
