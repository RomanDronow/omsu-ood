package patterns.creational.abstract_factory.factories;

import patterns.creational.abstract_factory.weapons.Archetype;
import patterns.creational.abstract_factory.weapons.Sword;
import patterns.creational.abstract_factory.weapons.Warrior;
import patterns.creational.abstract_factory.weapons.Weapon;

public class WarriorFactory implements RPGFactory {
    @Override
    public Archetype createArchetype() {
        return new Warrior();
    }

    @Override
    public Weapon createWeapon() {
        return new Sword();
    }
}
