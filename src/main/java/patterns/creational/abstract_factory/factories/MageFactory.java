package patterns.creational.abstract_factory.factories;

import patterns.creational.abstract_factory.weapons.Archetype;
import patterns.creational.abstract_factory.weapons.Mage;
import patterns.creational.abstract_factory.weapons.Wand;
import patterns.creational.abstract_factory.weapons.Weapon;


public class MageFactory implements RPGFactory {
    @Override
    public Archetype createArchetype() {
        return new Mage();
    }

    @Override
    public Weapon createWeapon() {
        return new Wand();
    }
}
