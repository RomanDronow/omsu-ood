package patterns.creational.abstract_factory.weapons;

public class Sword implements Weapon{

    @Override
    public void pick() {
        System.out.println("You're pick your sword");
    }
}
