package patterns.creational.abstract_factory.weapons;

public interface Weapon {
    void pick();
}
