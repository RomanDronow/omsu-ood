package patterns.creational.abstract_factory.weapons;

public class Wand implements Weapon {
    @Override
    public void pick() {
        System.out.println("You're take a wand from your back");
    }
}
