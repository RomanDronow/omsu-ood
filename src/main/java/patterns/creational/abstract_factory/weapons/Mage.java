package patterns.creational.abstract_factory.weapons;

public class Mage implements Archetype {
    @Override
    public void pick() {
        System.out.println("You chose a mage");
    }
}
