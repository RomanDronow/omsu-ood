package patterns.creational.abstract_factory.weapons;

public class Warrior implements Archetype{

    @Override
    public void pick() {
        System.out.println("You chose a warrior");
    }
}
