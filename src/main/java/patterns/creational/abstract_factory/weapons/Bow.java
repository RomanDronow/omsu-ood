package patterns.creational.abstract_factory.weapons;

public class Bow implements Weapon {
    @Override
    public void pick() {
        System.out.println("You're pick your bow and arrows");
    }
}
