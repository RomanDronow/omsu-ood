package patterns.creational.abstract_factory.weapons;

public class Archer implements Archetype{
    @Override
    public void pick() {
        System.out.println("You chose an archer");
    }
}
