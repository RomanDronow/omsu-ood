package patterns.creational.abstract_factory.weapons;

public interface Archetype {
    void pick();
}
