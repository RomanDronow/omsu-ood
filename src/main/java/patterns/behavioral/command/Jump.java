package patterns.behavioral.command;

public class Jump implements Command {
    public void execute() {
        System.out.println("Jump!");
    }
}
