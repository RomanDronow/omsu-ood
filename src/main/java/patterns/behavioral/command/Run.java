package patterns.behavioral.command;

public class Run implements Command{
    public void execute() {
        System.out.println("Run-run-run!");
    }
}
