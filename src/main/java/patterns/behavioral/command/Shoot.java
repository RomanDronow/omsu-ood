package patterns.behavioral.command;

public class Shoot implements Command {
    public void execute() {
        System.out.println("Pew-pew!");
    }
}
