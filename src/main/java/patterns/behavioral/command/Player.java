package patterns.behavioral.command;

public class Player {
    private Command run;
    private Command jump;
    private Command shoot;

    public Player(Command run, Command jump, Command shoot) {
        this.run = run;
        this.jump = jump;
        this.shoot = shoot;
    }

    public void run(){
        run.execute();
    }

    public void jump(){
        jump.execute();
    }

    public void shoot(){
        shoot.execute();
    }
}
