package patterns.behavioral.command;

public class Demo {
    public static void main(String[] args) {
        Player player = new Player(new Run(), new Jump(), new Shoot());
        player.run();
        player.jump();
        player.run();
        player.shoot();
    }
}
