package patterns.behavioral.memento;

public class Player {
    private int level;
    private String name;

    public Player(String name) {
        level = 1;
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    public void levelUp(){
        level++;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SaveFile save() {
        System.out.println("Saving...");
        return new SaveFile(level, name);
    }

    public void restore(SaveFile s) {
        System.out.println("Restoring...");
        level = s.level;
        name = s.name;
    }
}
