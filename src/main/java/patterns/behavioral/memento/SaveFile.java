package patterns.behavioral.memento;

public class SaveFile {
    int level;
    String name;

    public SaveFile(int level, String name) {
        this.level = level;
        this.name = name;
    }
}
