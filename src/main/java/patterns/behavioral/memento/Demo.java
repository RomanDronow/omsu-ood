package patterns.behavioral.memento;

public class Demo {
    public static void main(String[] args) {
        Player player1 = new Player("Artyom");
        player1.levelUp();
        player1.levelUp();
        player1.levelUp();
        System.out.printf("Player %s, level %s\n",player1.getName(),player1.getLevel());
        SaveFile s = player1.save();
        player1.levelUp();
        player1.levelUp();
        player1.levelUp();
        System.out.printf("Player %s, level %s\n",player1.getName(),player1.getLevel());
        player1.restore(s);
        System.out.printf("Player %s, level %s\n",player1.getName(),player1.getLevel());
    }
}
