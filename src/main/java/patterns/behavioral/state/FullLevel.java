package patterns.behavioral.state;

public class FullLevel implements Level {
    public static final int VOLUME = 100;

    @Override
    public int getVolume() {
        return VOLUME;
    }

    @Override
    public void charge(Device device) {
        System.out.println("\"Battery is full. Turn off charger.\"");
    }

    @Override
    public void discharge(Device device) {
        System.out.println("*Battery level decreasing...*");
    }
}
