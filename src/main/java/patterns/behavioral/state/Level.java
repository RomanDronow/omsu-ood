package patterns.behavioral.state;

public interface Level {
    int getVolume();
    void charge(Device device);
    void discharge(Device device);
}
