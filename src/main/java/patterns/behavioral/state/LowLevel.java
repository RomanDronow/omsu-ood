package patterns.behavioral.state;

public class LowLevel implements Level {
    public static final int VOLUME = 5;

    @Override
    public int getVolume() {
        return VOLUME;
    }

    @Override
    public void charge(Device device) {
        System.out.println("Device charging...");
    }

    @Override
    public void discharge(Device device) {
        System.out.println("*device turned off*");
    }
}
