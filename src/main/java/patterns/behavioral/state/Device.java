package patterns.behavioral.state;

public class Device {
    private Level batteryLevel;

    public void charge(){
        batteryLevel.charge(this);
    }

    public void discharge(){
        batteryLevel.discharge(this);
    }

    public Device(Level batteryLevel) {
        this.batteryLevel = batteryLevel;
    }
}
