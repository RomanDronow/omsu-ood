package patterns.behavioral.observer;

public class Performance {
    public static void main(String[] args) {
        Speaker speaker = new Speaker();
        Crowd crowd = new Crowd(speaker);
        speaker.setSpeech("Blah-blah-blah, something-something...");
    }
}
