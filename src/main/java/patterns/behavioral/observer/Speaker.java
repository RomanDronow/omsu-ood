package patterns.behavioral.observer;

import java.util.LinkedList;
import java.util.List;

public class Speaker implements Observable {
    private List<Observer> observers;
    private String speech;

    public Speaker() {
        observers = new LinkedList<>();
    }

    @Override
    public void registerObserver(Observer o) {
        observers.add(o);
    }

    @Override
    public void removeObserver(Observer o) {
        observers.remove(o);
    }

    @Override
    public void notifyObservers() {
        for (Observer observer : observers) {
            observer.update(speech);
        }
    }

    public void setSpeech(String speech) {
        this.speech = speech;
        notifyObservers();
    }
}
