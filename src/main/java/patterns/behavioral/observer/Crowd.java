package patterns.behavioral.observer;

public class Crowd implements Observer {
    private Speaker speaker;
    private String speech;

    public Crowd(Speaker speaker) {
        this.speaker = speaker;
        speaker.registerObserver(this);
    }

    @Override
    public void update(String speech) {
        this.speech = speech;
        listenSpeaker();
    }

    public void listenSpeaker() {
        System.out.println("The crowd listen speaker: \"" + speech + "\"");
    }
}
