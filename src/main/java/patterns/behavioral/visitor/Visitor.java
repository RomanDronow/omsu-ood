package patterns.behavioral.visitor;

public interface Visitor {
    String visitKitchen(Kitchen kitchen);
    String visitBathroom(Bathroom bathroom);
    String visitBedroom(Bedroom bedroom);
}
