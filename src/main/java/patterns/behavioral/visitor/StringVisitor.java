package patterns.behavioral.visitor;

public class StringVisitor implements Visitor {

    public String summary(Room... rooms) {
        StringBuilder sb = new StringBuilder();
        sb.append("List of rooms: \n\n");
        for (Room room: rooms) {
            sb.append(room.accept(this));
            System.out.println(sb.toString());
            sb.setLength(0);
        }
        return sb.toString();
    }

    @Override
    public String visitKitchen(Kitchen kitchen) {
        return kitchen.getName() + ", Square: " + kitchen.square();
    }

    @Override
    public String visitBathroom(Bathroom bathroom) {
        return bathroom.getName() + ", Square: " + bathroom.square();
    }

    @Override
    public String visitBedroom(Bedroom bedroom) {
        return bedroom.getName() + ", Square: " + bedroom.square();
    }
}
