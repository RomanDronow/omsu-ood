package patterns.behavioral.visitor;

public class Demo {
    public static void main(String[] args) {
        Room[] rooms = new Room[6];
        rooms[0] = new Kitchen(12,21);
        rooms[1] = new Bathroom(13,31);
        rooms[2] = new Bathroom(4,1);
        rooms[3] = new Bedroom(1,2);
        rooms[4] = new Kitchen(5,4);
        rooms[5] = new Bedroom(222,222);

        summary(rooms);


    }
    private static void summary(Room... rooms){
        StringVisitor sv = new StringVisitor();
        System.out.println(sv.summary(rooms));
    }
}
