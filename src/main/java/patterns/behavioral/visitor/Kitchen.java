package patterns.behavioral.visitor;

public class Kitchen extends Room {
    public Kitchen(int width, int length) {
        super(width, length);
        setName("Kitchen");
    }

    @Override
    public String accept(Visitor visitor) {
        return visitor.visitKitchen(this);
    }
}
