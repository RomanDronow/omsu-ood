package patterns.behavioral.visitor;

public abstract class Room {
    private int width;
    private int length;
    private String name;

    public Room(int width, int length) {
        this.width = width;
        this.length = length;
    }

    public int getWidth() {
        return width;
    }

    public int getLength() {
        return length;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public abstract String accept(Visitor visitor);

    public double square() {
        return length * width;
    }
}
