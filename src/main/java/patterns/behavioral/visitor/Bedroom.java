package patterns.behavioral.visitor;

public class Bedroom extends Room {
    public Bedroom(int width, int length) {
        super(width, length);
        setName("Bedroom");
    }

    @Override
    public String accept(Visitor visitor) {
        return visitor.visitBedroom(this);
    }
}
