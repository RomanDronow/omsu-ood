package patterns.behavioral.visitor;

public class Bathroom extends Room {
    public Bathroom(int width, int length) {
        super(width, length);
        setName("Bathroom");
    }

    @Override
    public String accept(Visitor visitor) {
        return visitor.visitBathroom(this);
    }
}
