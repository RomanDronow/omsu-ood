package patterns.behavioral.mediator;

public class Demo {
    public static void main(String[] args) {
        ConcreteMediator m = new ConcreteMediator();

        ConcretePerson bob = new ConcretePerson("Bob", m);
        ConcretePerson jack = new ConcretePerson("Jack", m);

        m.setFirst(bob);
        m.setSecond(jack);

        m.say("Hello!", bob);
        m.say("Howdy!", jack);
    }
}
