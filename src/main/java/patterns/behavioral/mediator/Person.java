package patterns.behavioral.mediator;

public abstract class Person {
    protected Mediator mediator;

    public Person(Mediator mediator) {
        this.mediator = mediator;
    }

    public void say(String message) {
        mediator.say(message, this);
    }

    public abstract void listen(String message);
}
