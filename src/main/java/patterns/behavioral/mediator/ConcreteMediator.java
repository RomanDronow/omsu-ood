package patterns.behavioral.mediator;

public class ConcreteMediator extends Mediator {
    private ConcretePerson first;

    private ConcretePerson second;

    public void setFirst(ConcretePerson first) {
        this.first = first;
    }

    public void setSecond(ConcretePerson second) {
        this.second = second;
    }

    @Override
    public void say(String message, Person person) {
        if (person.equals(second)) {
            second.tell(message);
            first.listen(message);
        } else {
            first.tell(message);
            second.listen(message);
        }
    }
}
