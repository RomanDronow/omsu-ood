package patterns.behavioral.mediator;

public abstract class Mediator {
    public abstract void say(String message, Person person);

}
