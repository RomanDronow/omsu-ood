package patterns.behavioral.mediator;

public class ConcretePerson extends Person{
    String name;
    public ConcretePerson(String name, Mediator mediator) {
        super(mediator);
        this.name = name;
    }

    @Override
    public void listen(String message) {
        System.out.println(name + " heard \"" + message + "\"");
    }

    public void tell(String message){
        System.out.println(name + " said \"" + message + "\"");
    }
}
