package patterns.behavioral.strategy;

public interface SortStrategy {
    public void sort(double... array);
}
