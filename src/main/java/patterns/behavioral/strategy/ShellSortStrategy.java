package patterns.behavioral.strategy;

public class ShellSortStrategy implements SortStrategy {
    @Override
    public void sort(double... array) {
        int n = array.length;

        int inc = 1;
        do {
            inc *= 3;
            inc++;
        } while (inc <= n);

        do {
            inc /= 3;
            for (int i = inc; i < n; i++) {
                double v = array[i];
                int j = i;
                while (array[j - inc] > v) {
                    array[j] = array[j - inc];
                    j -= inc;
                    if (j < inc) {
                        break;
                    }
                }
                array[j] = v;
            }
        } while (inc > 1);
    }
}