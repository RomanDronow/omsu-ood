package patterns.behavioral.strategy;

import java.util.Arrays;
import java.util.Collections;
import java.util.concurrent.ThreadLocalRandom;

public class Demo {
    public static void main(String[] args) {
        double[] array = new double[100000000];
        int s = 0;
        for (int i = array.length - 1; i >= 0; i--) {
            array[i] = ThreadLocalRandom.current().nextDouble();
            s++;
        }
        long init = System.nanoTime();
        SortStrategy bubble = new BubbleSortStrategy();
        SortStrategy merge = new MergeSortStrategy();
        SortStrategy shell = new ShellSortStrategy();
        shell.sort(array);
        System.out.println(((double) System.nanoTime()-init)/1e9);
    }
}
