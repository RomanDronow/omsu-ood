package patterns.behavioral.chain_of_responsibility;

public class SecondNotifier extends Notifier {
    public SecondNotifier(int priority) {
        super(priority);
    }

    @Override
    public void send(String message) {
        System.out.println("Second level: " + message);
    }
}
