package patterns.behavioral.chain_of_responsibility;

public abstract class Notifier {
    private int priority;
    private Notifier next;

    public Notifier(int priority) {
        this.priority = priority;
    }

    public void setNextNotifier(Notifier next) {
        this.next = next;
    }

    public void notifyManager(String message, int level){
        if(level>=priority){
            send(message);
        }
        if(next != null){
            next.notifyManager(message,level);
        }
    }

    public abstract void send(String message);
}
