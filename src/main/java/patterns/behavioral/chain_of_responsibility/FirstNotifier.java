package patterns.behavioral.chain_of_responsibility;

public class FirstNotifier extends Notifier {
    public FirstNotifier(int priority) {
        super(priority);
    }

    @Override
    public void send(String message) {
        System.out.println("First level: " + message);
    }
}
