package patterns.behavioral.chain_of_responsibility;

public class ThirdNotifier extends Notifier {
    public ThirdNotifier(int priority) {
        super(priority);
    }

    @Override
    public void send(String message) {
        System.out.println("Third level: " + message);
    }
}
