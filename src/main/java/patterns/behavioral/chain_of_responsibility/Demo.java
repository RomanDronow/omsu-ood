package patterns.behavioral.chain_of_responsibility;

public class Demo {
    public static void main(String[] args) {
        Notifier first = new FirstNotifier(1);
        Notifier second = new SecondNotifier(2);
        Notifier third = new ThirdNotifier(3);

        first.setNextNotifier(second);
        second.setNextNotifier(third);

        first.notifyManager("This message to level 1",1);
        System.out.println();
        first.notifyManager("This message to level 2",2);
        System.out.println();
        first.notifyManager("This message to level 3",3);
    }
}
