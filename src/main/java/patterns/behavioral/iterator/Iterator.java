package patterns.behavioral.iterator;

public class Iterator{
    private Storage storage;
    private int cursor;
    private boolean finished;

    public Iterator(Storage storage) {
        this.storage = storage;
        cursor = 0;
        finished = false;
    }

    public Item getCurrent() {
        return storage.getItems()[cursor];
    }

    public void next() {
        if (!finished) {
            cursor++;
            if (cursor == storage.getItems().length) {
                finished = true;
            }
        }
    }

    public boolean isFinished() {
        return finished;
    }
}
