package patterns.behavioral.iterator;

public class Storage {
    private Item[] items;

    public Storage(Item ... items) {
        this.items = items;
    }

    public Item[] getItems() {
        return items;
    }
}
