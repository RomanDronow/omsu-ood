package patterns.behavioral.iterator;

public class Demo {
    public static void main(String[] args) {
        Item axe = new Item("axe", 1);
        Item rifle = new Item("rifle",1);
        Item bullets = new Item("bullets", 60);
        Item suit = new Item("suits", 2);

        Storage chest = new Storage(axe,rifle,bullets,suit);

        Iterator itr = new Iterator(chest);
        System.out.println("Chest contains:");
        while (!itr.isFinished()){
            System.out.println(itr.getCurrent().getCount() + " " + itr.getCurrent().getName());
            itr.next();
        }
    }
}
